// copyright Jezra 2022
// released GPLv3

package config

import (
  "log"
  "io/ioutil"
  "encoding/json"
)

//define the Config structure
type Config struct{
  EnergizePin int
  PolarityPin int
  TogglePin int
  OpenDelayMinutes int
  CloseDelayMinutes int
  EnergizedSeconds int
  Lat float64
  Lng float64
}

func New(appDir string) Config {
  /* config will be in same dir as executable */
  log.Printf("Looking for config in %v\n", appDir )
  data, err := ioutil.ReadFile(appDir+"/config.json")
  if err != nil {
    log.Fatal("Config: %s", err)
  }
  //create a var to hold the json object
  var conf Config
  // unmarshall it
  err = json.Unmarshal(data, &conf)
  if err != nil {
    log.Fatal("Config: %s", err)
  }
  //return the configuration
  return conf
}