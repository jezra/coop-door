/* copyright 2020 Jezra
released under GPLv3
*/

package ioPin

import (
  "log"
  "fmt"
  "io/ioutil"
  )

type IoPin struct {
  direction string
  number int
}

func writeToFile(file string, txt string) {
  err := ioutil.WriteFile(file, []byte(txt), 0644)
  if err != nil {
    log.Print(err)
  }
}

func New(direction string, number int) IoPin {
  //make an instance of pin struct
  pin := IoPin{direction, number}
  //unexport
  writeToFile("/sys/class/gpio/unexport", fmt.Sprintf("%v",number) )
  
  //export
  writeToFile("/sys/class/gpio/export", fmt.Sprintf("%v",number) )
  //set direction
  writeToFile(fmt.Sprintf("/sys/class/gpio/gpio%d/direction", number), direction)
  return pin
}

func (pin IoPin)Value() int64 {
  var val int64
  content, err := ioutil.ReadFile( fmt.Sprintf("/sys/class/gpio/gpio%d/value",pin.number) )
  if err != nil {
    //unable to read
    return -1
  }
  //content is a []byte, in this case ASCII 1 or 0, followed by line-feed: [48, 10] or [49,10]
  if content[0]==48 {
    val = 0
  } else if content[0]==49{
    val = 1
  }
  return val
}

func (pin IoPin)SetHigh(){
  writeToFile(fmt.Sprintf("/sys/class/gpio/gpio%d/value",pin.number), "1")
  
}
func (pin IoPin)SetLow(){
   writeToFile(fmt.Sprintf("/sys/class/gpio/gpio%d/value",pin.number), "0")
}

func (pin IoPin)On(){
  pin.SetLow()
}

func (pin IoPin)Off(){
  pin.SetHigh()
}