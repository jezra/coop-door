/* copyright 2020 Jezra
released under GPLv3
*/
package timer

import(
  "time"
  "log"
  "fmt"
  "server/sunCalc"
)

// track timed actions
type timedAction struct {
  time string //HH:MM format 
  action string
}

var globalTimedActions []timedAction
var globalTimes map[string]string
var channel chan string
var quit bool
var calc sunCalc.SunCalc
var lat, lng float64
var oDelay int
var cDelay int

func Run(openDelay int, closeDelay int, energizedSeconds int, latitude float64, longitude float64) (chan string) {
  lat = latitude
  lng = longitude
  oDelay = openDelay
  cDelay = closeDelay
  channel = make(chan string)
  globalTimedActions = make([]timedAction,0)
  globalTimes = make(map[string]string)
  //computeActionTimes at 3AM
  ta := timedAction{time:"3:00",action: "compute-times"}
  // add the action to the array
  globalTimedActions = append(globalTimedActions, ta)
  calc = sunCalc.New()
  //compute the times
  computeActionTimes()
  
  //start running
  go runloop()

  //return the channel
  return channel
}

func runloop(){
  // sleep
  sleepTime := time.Second * 30
  //infinite loop
  for true {
    //check the actions
    checkActions()
    //sleep for some time
    time.Sleep(sleepTime)
  }
}

func addAction(when string, action string) {
  //make a new timed Action
  ta := timedAction{time: when, action: action}
  //append to the global timedActions
  globalTimedActions = append(globalTimedActions, ta)
}

// return the globalTimes for door actions
func Times() map[string]string{
  times := globalTimes
  times["currentTime"] = timeInHM( time.Now() )
  //loop through the remaing globalTimedActions
  /*
  for i := range globalTimedActions {
    timedAction := globalTimedActions[i]
    times[fmt.Sprintf("Pending: %v",timedAction.time)] = timedAction.action
  }*/
  return times
}

func checkActions(){
  // get the time now in HH:MM string format
  now := time.Now()
  hm := timeInHM(now)
  //var foundIndex int
  foundIndex := -1
  //loop through the globalTimedActions
  for i:=0 ; i < len(globalTimedActions) ; i++ {
    gta := globalTimedActions[i]
    // is the time of the gta the same as the hm computed earlier?
    if hm == gta.time {
      foundIndex = i
    }
  }
  if foundIndex >= 0 {
    // process the action
    processTimedActionByIndex(foundIndex)
  }
}

// receive the 'action' string of a timedAction as input
func processTimedActionByIndex(index int) {
  //default, we want to remove the action
  //get the timedAction
  action := globalTimedActions[index].action
  log.Printf("Processing Action: %v", action)
  removeAction := true
  switch action {
    case "compute-times":
      computeActionTimes()
      //do not delete the action
      removeAction = false
    case "open-door":
      //do open door stuff
      channel <- "open"
    case "close-door":
      //do close door stuff
      channel <- "close"
    default: 
      //do nothing 
  }
  if removeAction {
    /* remove the indexed item from the slice */
    //replace index item with last item
    globalTimedActions[index] = globalTimedActions[len(globalTimedActions)-1]
    //truncate to remove the last item
    globalTimedActions = globalTimedActions[:len(globalTimedActions)-1]
  }
}

func timeInHM(t time.Time) string {
  // get the minutes from the time and zero pad if necessary
   minutes := t.Minute()
   minuteString := fmt.Sprintf("%d",minutes)
   if minutes < 10 {
     minuteString = fmt.Sprintf("0%d",minutes) 
   }
  return fmt.Sprintf("%d:%s",t.Hour(), minuteString)
}

func computeActionTimes(){
  // get the times 
  times := calc.GetTimes( time.Now(), lat, lng)
  
  // open 1 hour after sunrise
  openTime := times["sunriseEnd"].Add(time.Minute*time.Duration(oDelay))
  openCheckTime := openTime.Add(time.Minute*3)
  
  ot := timeInHM(openTime)
  oct := timeInHM(openCheckTime)
  
  closeTime := times["dusk"]
  closeCheckTime := closeTime.Add(time.Minute*time.Duration(cDelay))
  
  ct := timeInHM(closeTime)
  cct := timeInHM(closeCheckTime)
  
  // add the timed actions to the queue
  addAction(ot, "open-door")
  addAction(oct, "open-door")
  addAction(ct, "close-door")
  addAction(cct, "close-door")
  
  //track the times in the globalTimes
  globalTimes["open"] = ot
  globalTimes["openCheck"] = oct 
  globalTimes["close"] = ct
  globalTimes["closeCheck"] = cct
  
}

