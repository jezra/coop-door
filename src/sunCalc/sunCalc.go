// Licensed 2-Clause BSD
// ported from https://github.com/mourner/suncalc

package sunCalc

import (
	"math"
	"time"
)

type SunCalc struct{
	// does anything need to be stored?
}

var PI, rad, e, j0 float64
var dayMs, j1970, j2000 int64

//create an array or stucts to hold time data
//var times 


//start a new sunCalc instance?
func New() SunCalc {
	// set the global variables
	PI = math.Pi
	rad = PI / 180
	j1970 = 2440588
	j2000 = 2451545
	dayMs = 1000 * 60 * 60 * 24
	e = rad * 23.4397
	j0 = 0.0009
	calc := SunCalc{}
	return calc
}


func toJulian(date time.Time) float64{
	//get unixTime in milliseconds as a float
	unixTime := float64(date.Unix()*1000)
	julian := unixTime / float64(dayMs) - 0.5 + float64(j1970)
	return julian
}

func fromJulian(j float64) time.Time{
	num := ((j + 0.5 - float64(j1970)) * float64(dayMs))/1000
	return time.Unix( int64(num), 0)
}

func toDays(date time.Time) float64 {
	days := toJulian(date) - float64(j2000)
	return days
}


/* position calculations */

func rightAscension(l float64, b float64) float64{
	return math.Atan2(math.Sin(l) * math.Cos(e) - math.Tan(b) * math.Sin(e), math.Cos(l))
}

func declination(l float64, b float64) float64 {
	return math.Asin(math.Sin(b) * math.Cos(e) + math.Cos(b) * math.Sin(e) * math.Sin(l))
}

func azimuth(H float64, phi float64, dec float64) float64 {
	return math.Atan2(math.Sin(H), math.Cos(H) * math.Sin(phi) - math.Tan(dec) * math.Cos(phi))
}

func altitude(H float64, phi float64, dec float64) float64 {
	return math.Asin(math.Sin(phi) * math.Sin(dec) + math.Cos(phi) * math.Cos(dec) * math.Cos(H))
}

func siderealTime(d float64, lw float64) float64{
	return rad * (280.16 + 360.9856235 * d) - lw
}

func astroRefraction(h float64) float64{
  if h < 0 {// the following formula works for positive altitudes only.
      h = 0 // if h = -0.08901179 a div/0 would occur.
  }
  // formula 16.4 of "Astronomical Algorithms" 2nd edition by Jean Meeus (Willmann-Bell, Richmond) 1998.
  // 1.02 / tan(h + 10.26 / (h + 5.10)) h in degrees, result in arc minutes -> converted to rad:
  return 0.0002967 / math.Tan(h + 0.00312536 / (h + 0.08901179))
}

// more sun calculations 


func solarMeanAnomaly(d float64) float64{
	return rad * (357.5291 + 0.98560028 * d)
}

func eclipticLongitude(m float64) float64{
  var c = rad * (1.9148 * math.Sin(m) + 0.02 * math.Sin(2 * m) + 0.0003 * math.Sin(3 * m)) // equation of center
  var p = rad * 102.9372; // perihelion of the Earth
  return m + c + p + PI;
}

func sunCoords(d float64) (float64,float64){
  var m = solarMeanAnomaly(d)
  var l = eclipticLongitude(m)
  return declination(l, 0), rightAscension(l, 0)
}

// calcs for sun times 

func julianCycle(d float64, lw float64) float64{
	return math.Round(d - j0 - lw / (2 * PI))
}

func approxTransit(ht float64, lw float64, n float64) float64{
	return j0 + (ht + lw) / (2 * PI) + n
}

func solarTransitJ(ds float64, m float64, l float64) float64 {
	return float64(j2000) + ds + 0.0053 * math.Sin(m) - 0.0069 * math.Sin(2 * l)
}

func hourAngle(h float64, phi float64, d float64) float64{
	return math.Acos((math.Sin(h) - math.Sin(phi) * math.Sin(d)) / (math.Cos(phi) * math.Cos(d)))
}

// returns set time for the given sun altitude
func getSetJ(h float64, lw float64, phi float64, dec float64, n float64, m float64, l float64) float64{
	w := hourAngle(h, phi, dec)
  a := approxTransit(w, lw, n)
  return solarTransitJ(a, m, l);
}

// calculates sun times for a given date and latitude/longitude

func (sc SunCalc)GetTimes(date time.Time, lat float64, lng float64) map[string]time.Time {
	sunTimes := []struct {
		angle float64
		morningName string
		eveningName string
	}{
		{-0.833,"sunrise","sunset"},
		{-0.3,"sunriseEnd","sunsetStart"},
		{-6,"dawn","dusk"},
		{-12,"nauticalDawn","nauticalDusk"},
		{-18,"nightEnd","night"},
		{6,"goldenHourEnd","goldenHour"},
	}
	result := make(map[string]time.Time)
	
  lw := rad * -lng
  phi := rad * lat

  d := toDays(date)
  n := julianCycle(d, lw)
  ds := approxTransit(0, lw, n)

  m := solarMeanAnomaly(ds)
  l := eclipticLongitude(m)
  dec := declination(l, 0)

	jnoon := solarTransitJ(ds, m, l)
	
	result["solarNoon"] = fromJulian(jnoon)
	result["nadir"] = fromJulian(jnoon - 0.5)
	
	for i := 0; i < len(sunTimes) ; i ++ {
		sunTime := sunTimes[i]
		
		jset := getSetJ(sunTime.angle * rad, lw, phi, dec, n, m, l)
		jrise := jnoon - (jset - jnoon)
		
		result[sunTime.morningName] = fromJulian(jrise)
		result[sunTime.eveningName] = fromJulian(jset)
	}
	return result
}