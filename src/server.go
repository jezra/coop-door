/* copyright 2020 Jezra
released under GPLv3
*/

package main

import (
  "fmt"
  "os"
  "net/http"
  "log"
  "time"
  "path/filepath"
  "encoding/json"
  "server/ioPin"
  "server/timer"
  "server/config"
)

// keep IO pins in a global array TODO:// ioPins as map of 'energize', 'polarity', 'toggle'
var ioPins [3]ioPin.IoPin
var state string
var energized bool
var publicDir string
var conf config.Config

func openDoor(){
  log.Printf("Opening Door")    
  state = "opening"
  energized = true
  //energize both pins
  ioPins[0].On()
  ioPins[1].On()
  //wait a lil bit
  time.Sleep(time.Duration(conf.EnergizedSeconds)*time.Second)
  //de-energize
  ioPins[0].Off()
  ioPins[1].Off()
  state = "open"
  energized = false
}

func closeDoor(){
  log.Printf("Closing Door")    
  energized = true
  state="closing"
  //energize the 1 pins
  ioPins[1].On()
  //wait a lil bit
  time.Sleep(time.Duration(conf.EnergizedSeconds)*time.Second)
  //de-energize
  ioPins[1].Off()
  state="closed"
  energized=false
}

func handleRoot(w http.ResponseWriter, r *http.Request) {
  path := r.URL.Path
  //if the path is "/", serve the index.html file 
  if path=="/" {
    path="/index.html"
  }
  /* TODO: better sanitizing of the path */
  var newPath = filepath.Join(publicDir, path)
  http.ServeFile(w,r,newPath)
}

func handleOpen(w http.ResponseWriter, r *http.Request) {
  //open the door if the door is not energized
  if !energized {
    go openDoor()
  }
  time.Sleep(100*time.Millisecond)
  fmt.Fprintf(w, state) // send data to client side
}

func handleClose(w http.ResponseWriter, r *http.Request) {
  //open the door if not energized
  if !energized {
    go closeDoor()
  } 
  time.Sleep(100*time.Millisecond)
  fmt.Fprintf(w, state) // send data to client side
}

func handleState(w http.ResponseWriter, r *http.Request) {
  status := timer.Times()
  status["state"] = state
  json, err := json.Marshal(status)
  if err!=nil {
   fmt.Fprint(w, "error")
  } else {
    fmt.Fprintf(w, string(json)) // send data to client side
  }
}

func timerChannelLoop(channel chan string) {
  //infinite loop
  for true {
    //block waiting for date from the channel
    direction := <- channel
    if direction == "close"{
      closeDoor()
    } else if direction == "open" {
      openDoor()
    }
  }
}

func pollToggleLoop() {
  var currentValue int64 = 0
  //infinite loop
  for true {
    //sleep for a bit
    time.Sleep(100*time.Millisecond)
    //check the value of iopin 2
    val := ioPins[2].Value()
    //did the value change?
    if val != currentValue {
      //update current value
      currentValue = val
      //was the toggle pressed?
      if val == 1 {
        // if state is opened or closed, toggle the door
        if state == "open" || state=="init" {
          closeDoor()
        } else if state == "closed" {
          openDoor()  
        }
      }
    }
  }
}



/* the main */
func main() {
  // get the apps directory
  appDir, dErr := filepath.Abs(filepath.Dir(os.Args[0]))
  if dErr != nil {
    log.Fatal(dErr)
  }

  // read config
  conf = config.New(appDir)
  //run the timer and get its channel
  channel := timer.Run(conf.OpenDelayMinutes, conf.CloseDelayMinutes, conf.EnergizedSeconds, conf.Lat, conf.Lng)
    // go routine the channelLoop
  go timerChannelLoop(channel)
  
  // state is unknown until a command has been given
  state="init"
  //public dir (where to look for static assets)
  publicDir = "public"
  //set up the pins 
  
  ioPins[0] = ioPin.New("out",conf.PolarityPin)
  ioPins[1] = ioPin.New("out",conf.EnergizePin)
  ioPins[2] = ioPin.New("in",conf.TogglePin)
  
  // ensure pins are off
  ioPins[0].Off()
  ioPins[1].Off()
  
  go pollToggleLoop()
  
  const PORT = 9090
  http.HandleFunc("/", handleRoot) // set root handler
  http.HandleFunc("/open", handleOpen) // set open handler
  http.HandleFunc("/close", handleClose) // set close handler
  http.HandleFunc("/state", handleState) // set state handler
  
  //inform the user of serving
  log.Printf("Serving on port: %v", PORT)
  //start serving
  err := http.ListenAndServe(fmt.Sprintf(":%v",PORT), nil) // set listen port
  if err != nil {
    log.Fatal("ListenAndServe: ", err)
  }
}

