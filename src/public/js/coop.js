/* global fetch */
/* global DOORSTATE */
let DOORSTATE = null;
//close the door
function closeDoor() {
  fetch('close');
  checkState();
}
function openDoor() {
  fetch('open');
  checkState();
}

function buttonClicked(){
  console.log(DOORSTATE);
  if (DOORSTATE=="open" || DOORSTATE=="init") {
    closeDoor();
  }else if (DOORSTATE=="closed"){
    openDoor();
  }
}

//find by id helper
function $id(elementId) {
  return document.getElementById(elementId);
}

function stateChange(state){
  //did the state actually change?
  if (DOORSTATE != state) {
    //the state has changed
    DOORSTATE = state;

    // set some defaults for the button
    var btnText = "close";
    var btnDisabled = false;
    if (state == "closing" || state == "opening" ) {
      //update the text
      btnText = state;
      btnDisabled = true;
    } else if (state == "closed"){
      btnText = "open";
    }
    // should the button be disabled?
    $id('theButton').disabled = btnDisabled;
    $id('theButton').textContent = btnText;
  }
}

// get the state from the API
function checkState() {
  //fetch the state
  fetch("/state").then(resp=>{
    return resp.text().then(txt=>{
      //parse the text as json
      let jsn = JSON.parse(txt);
      //handle the state changing
      stateChange(jsn['state']);
      
      //get the keys from the json object
      let keys = Object.keys(jsn);
      //loop through the keys 
      keys.forEach(k =>{
        let element = $id(`status-${k}`);
        if (element) {
          element.innerHTML = jsn[k];
        }
        
      });
    });
  });
}

function looper() {
 // call looper in 1 second
 setTimeout(looper, 1000);
 //get the state
 checkState();
}


//start the looper
looper();