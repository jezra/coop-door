# Chicken Coop Door

The sofware that runs on a MIPS computer to control my chicken coop door.
Ducks and guineas also use the coop, so maybe it is just a fowl door.

See: https://jezra.net/post/2020-01-21_chicken-coop-upgrade.html

## Compiling

* cd to src
* `go build server.go`

the resulting binary will look for a config.json file in the same directory


